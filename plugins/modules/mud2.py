#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.mud_ut import utu


def main():
    utu()
    mod_args = {}
    mod = AnsibleModule(argument_spec=mod_args)
    resp = {"hello": "world"}
    mod.exit_json(changed=False, meta=resp)


if __name__ == "__main__":
    main()